// Nightscout Widget
//
// Copyright (C) 2020 by niepi <niepi@niepi.org>
//
// Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
// INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
// IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
// OF THIS SOFTWARE.

const myUrl = `https://your-nightscout-url`; // your nightscout url
const myToken = `your-nightscout-token`; // your nightscoutaccess token
const glucoseDisplay = `mgdl`;
//const glucoseDisplay = `mmoll`;
const dateFormat = `en-US`;
const testing = true;
const title = `MY GLUCOSE`;

// Initialize Widget
let widget = await createWidget();
if (!config.runsInWidget) {
    await widget.presentSmall();
}

Script.setWidget(widget);
Script.complete();

// Build Widget
async function createWidget(items) {
    const list = new ListWidget();

    let header, glucose, iob, cob, updated;

    let nsDataV2 = await getNsDataV2(myUrl, myToken);

    // create direction arrow
    let directionString = await getDirectionString(nsDataV2.direction);

    header = list.addText(title.toUpperCase());
    header.font = Font.mediumSystemFont(10);

    let glucoseValue = nsDataV2.bg;

    if (glucoseDisplay === `mmoll`) {
        glucoseValue = Math.round(nsDataV2.bg / 18 * 100) / 100;
    }

    glucose = list.addText("" + glucoseValue);
    glucose.font = Font.mediumSystemFont(34);

    let emoji;
    let changeBG = false;

    // Standard ranges
    if (glucoseValue < 69) {
        emoji = "🧃";
        changeBG = true;
    } else if (glucoseValue <= 150) {
        emoji = "👍🏻";
    } else if (glucoseValue < 180) {
        emoji = "🫣";
    } else if (glucoseValue < 250) {
        emoji = "🥴";
        changeBG = true;
    } else {
        emoji = "😵‍💫";
        changeBG = true;
    }

    // Special cases
    if (glucoseValue == 100) {
        emoji = "🦄";
    } else if (glucoseValue == 90) {
        emoji = "🎯";
    }

    // Change BG to yellow if action is needed (insulin or juice)
    if (changeBG) {
        list.backgroundColor = Color.orange();
    }

    emojiLine = list.addText("" + emoji);
    emojiLine.font = Font.mediumSystemFont(30);

    iob_cob = list.addText("I " + nsDataV2.iob + " / C" + nsDataV2.cob + " / " + directionString);
    iob_cob.font = Font.mediumSystemFont(10);

    let updateTime = new Date(nsDataV2.mills).toLocaleTimeString(dateFormat, { hour: "numeric", minute: "numeric" })
    updated = list.addText("" + updateTime);
    updated.font = Font.mediumSystemFont(10);


    list.refreshAfterDate = new Date(Date.now() + 60);
    return list;
}

async function getNsDataV2(nsUrl, nsToken) {
    if (testing) {
        return {
            bg: 85,
            direction: 'NONE',
            iob: 123,
            cob: 456,
            mills: Date.now()
        };
    }

    let url = nsUrl + "/api/v2/properties?&token=" + nsToken;
    let data = await new Request(url).loadJSON();

    return {
        bg: data.bgnow.mean,
        direction: data.bgnow.sgvs[0].direction,
        iob: data.iob.displayLine,
        cob: data.cob.displayLine,
        mills: data.bgnow.mills
    };
}


async function getDirectionString(direction) {

    let directionString
    switch (direction) {
        case 'NONE':
            directionString = '⇼';
            break;
        case 'DoubleUp':
            directionString = '⇈';
            break;
        case 'SingleUp':
            directionString = '↑';
            break;
        case 'FortyFiveUp':
            directionString = '↗';
            break;
        case 'Flat':
            directionString = '→';
            break;
        case 'FortyFiveDown':
            directionString = '↘';
            break;
        case 'SingleDown':
            directionString = '↓';
            break;
        case 'DoubleDown':
            directionString = '⇊';
            break;
        case 'NOT COMPUTABLE':
            directionString = '-';
            break;
        case 'RATE OUT OF RANGE':
            directionString = '⇕';
            break;
        default:
            directionString = '⇼';
    }
    return directionString;
}
