# scriptable-nightscout-widget

![widget](widget.jpeg)

Widget for scriptable.app to display nightscout data on an iOS device
with emojis and color changing when action is needed.

# update
- 06.04.2022 - added kid-friendly edits
- 27.10.2020 - readded update time
- 27.10.2020 - add option to show glucose in mmol/l
- 26.10.2020 - switches to nightscout v2 api

# install
- download https://scriptable.app 
- create a new script and copy nightscout.js content to the new script, or export script file from e.g. Dropbox and open in scriptable
- replace ```myUrl``` and ```myToken``` with your data
- change ```testing``` to ```false```
- change ```defaultBG``` and ```alertBG``` to [hex color codes](https://www.w3schools.com/colors/colors_picker.asp) desired
- set ```glucosDisplay``` to `mmoll` to show glucose in mmol/l
- change ```dateFormat```  to `de-DE` to show update time in german
- add scriptable widget to your iOS device, edit widget, and choose your script

